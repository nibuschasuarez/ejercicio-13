import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';

@Component({
  selector: 'app-caja',
  templateUrl: './caja.component.html',
  styleUrls: ['./caja.component.css']
})
export class CajaComponent implements OnInit {

  text: string = 'Escribir Correo Electronico...';
  texto: string= ''
  mensajeValido: string = 'El correo electrónico es válido';
  advertencia: string = 'El correo electrónico no es válido'
  form!: FormGroup;

  constructor(private fb: FormBuilder) { 
    this.crearForm();
  }



  ngOnInit(): void {
  }

  crearForm():void{
    this.form = this.fb.group({
      correo: ['', [Validators.required, Validators.pattern(/^(([^<>()\[\]\.,;:\s@\”]+(\.[^<>()\[\]\.,;:\s@\”]+)*)|(\”.+\”))@(([^<>()[\]\.,;:\s@\”]+\.)+[^<>()[\]\.,;:\s@\”]{2,})$/)] ]
    })
  }

  borrarPlaceholder(): void{
    this.text = ''
  }

  validOinvalid(): void {
    if (this.form.get('correo')?.valid){
      this.texto = this.mensajeValido
    } else {
      this.texto = this.advertencia
    }
  }

  guardar (){
    console.log('guardar');
    
  }
}
